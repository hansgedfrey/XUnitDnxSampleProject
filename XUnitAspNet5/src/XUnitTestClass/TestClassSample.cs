﻿

using Xunit;

namespace XUnitTestClass
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class TestClassSample
    {
        public TestClassSample()
        {
         
        }

         [Fact]
        public void TestAdd()
        { 
            Assert.Equal(4, Add(2, 2));
        }

        int Add(int x, int y)
        {
            return x + y;
        }
    }
}
